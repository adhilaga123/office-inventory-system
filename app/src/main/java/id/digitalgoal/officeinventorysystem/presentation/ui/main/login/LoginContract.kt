package id.digitalgoal.officeinventorysystem.presentation.ui.main.login

import id.digitalgoal.officeinventorysystem.presentation.util.base.BasePresenter
import id.digitalgoal.officeinventorysystem.presentation.util.base.BaseView

interface LoginContract {

    interface View : BaseView {

    }

    interface Presenter : BasePresenter {

    }
}