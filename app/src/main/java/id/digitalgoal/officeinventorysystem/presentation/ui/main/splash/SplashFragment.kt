package id.digitalgoal.officeinventorysystem.presentation.ui.main.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import id.digitalgoal.officeinventorysystem.R
import id.digitalgoal.officeinventorysystem.databinding.FragmentSplashBinding
import id.digitalgoal.officeinventorysystem.presentation.util.base.BaseFragment

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding, SplashContract.Presenter>(),
    SplashContract.View {

    override val layoutResourceId: Int = R.layout.fragment_splash
    override val presenter: SplashContract.Presenter by viewModels<SplashViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }



}