package id.digitalgoal.officeinventorysystem.presentation.ui.main.splash

import id.digitalgoal.officeinventorysystem.presentation.util.base.BasePresenter
import id.digitalgoal.officeinventorysystem.presentation.util.base.BaseView

interface SplashContract {

    interface View : BaseView {

    }

    interface Presenter : BasePresenter {

    }

}