package id.digitalgoal.officeinventorysystem.presentation.ui.main.splash

import id.digitalgoal.officeinventorysystem.di.datasource.Repository
import id.digitalgoal.officeinventorysystem.domain.datasource.UserDataSource
import id.digitalgoal.officeinventorysystem.presentation.util.base.BaseViewModel
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    @Repository private val userDataSource: UserDataSource
) : BaseViewModel(userDataSource), SplashContract.Presenter {
}