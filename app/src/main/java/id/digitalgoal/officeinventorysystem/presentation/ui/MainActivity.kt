package id.digitalgoal.officeinventorysystem.presentation.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import id.digitalgoal.officeinventorysystem.R
import id.digitalgoal.officeinventorysystem.databinding.ActivityMainBinding
import id.digitalgoal.officeinventorysystem.presentation.util.base.BaseActivity

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Inventory_User)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
    }

}