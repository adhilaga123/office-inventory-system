package id.digitalgoal.officeinventorysystem.di.datasource

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
//import id.digitalgoal.officeinventorysystem.data.AccountRepository
//import id.digitalgoal.officeinventorysystem.data.DashboardRepository
//import id.digitalgoal.officeinventorysystem.data.ReportRepository
//import id.digitalgoal.officeinventorysystem.domain.datasource.AccountDataSource
//import id.digitalgoal.officeinventorysystem.domain.datasource.DashboardDataSource
//import id.digitalgoal.officeinventorysystem.domain.datasource.ReportDataSource
import javax.inject.Qualifier

@Qualifier
annotation class Repository

@Qualifier
annotation class LocalDataSource

@Qualifier
annotation class RemoteDataSource

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

//    @Binds
//    @Repository
//    abstract fun bindAccountDataSource(accountRepository: AccountRepository): AccountDataSource
//
//    @Binds
//    @Repository
//    abstract fun bindDashboardDataSource(dashboardRepository: DashboardRepository): DashboardDataSource
//
//    @Binds
//    @Repository
//    abstract fun bindReportDataSource(reportRepository: ReportRepository): ReportDataSource

}