package id.digitalgoal.officeinventorysystem.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
//import id.digitalgoal.officeinventorysystem.data.remote.AppRemoteClient
//import id.digitalgoal.officeinventorysystem.remote.service.AccountService
import javax.inject.Qualifier

@Qualifier
annotation class FakeService

@Qualifier
annotation class LiveService

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

//    @Provides
//    @LiveService
//    fun provideAccountService(appRemoteClient: AppRemoteClient): AccountService {
//        return appRemoteClient.create(AccountService::class.java)
//    }

}